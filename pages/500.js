// pages/500.js
import Head from 'next/head'

import Thumbnail from '../public/Thumbnail.png'
import FaviconApple from '../public/apple-icon-180x180.png'

export default function Custom404() {
    return (
        <>
            <Head>
                <title>500 &mdash; Advertising First Media</title>
                <link rel="shortcut icon" href={FaviconApple} />
                <link rel="apple-touch-icon" href={FaviconApple} />
                <link rel="image_src" href={Thumbnail} /> 
                <link rel="search" type="application/opensearchdescription+xml" title="Advertising First Media" href="/opensearch.xml" />
                <meta name="description" content="Yuk pasang iklan di Advertising First Media sekarang! Gunakan berbagai fitur baru serta tampilan yang lebih user friendly. Buat member lama Solusi UKM Advertising First Media, tenang aja kamu masih bisa pasang iklan di Advertising First Media dengan akun lama kamu."/>
                <link rel="canonical" href="https://advertising.firstmedia.com" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta property="og:type" content= "website" />
                <meta property="og:url" content={Thumbnail}/>
                <meta property="og:site_name" content="Advertising First Media" />
                <meta property="og:image" itemprop="image primaryImageOfPage" content="/Thumbnail.png" />
                <meta name="twitter:card" content="summary"/>
                <meta name="twitter:domain" content="https://advertising.firstmedia.com"/>
                <meta name="twitter:title" property="og:title" itemprop="name" content="Advertising First Media" />
                <meta name="twitter:description" property="og:description" itemprop="description" content="Yuk pasang iklan di Advertising First Media sekarang! Gunakan berbagai fitur baru serta tampilan yang lebih user friendly. Buat member lama Solusi UKM Advertising First Media, tenang aja kamu masih bisa pasang iklan di Advertising First Media dengan akun lama kamu." />
                <link rel="icon" href="/apple-icon-180x180.png" />
                <link href="https://unpkg.com/@tailwindcss/forms/dist/forms.min.css" rel="stylesheet" />
                <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet" />
            </Head>
            <main className="flex flex-col px-0 py-0 mt-40">
                <div className="content" role="main">
                    <article className="hentry text-center">
                        <h1 className='text-5xl font-bold'>500 - Server-side error occurred</h1>
                        <p className='mr-64 ml-64 mt-5'>You must have picked the wrong door because I haven't been able to lay my eye on the page you've been searching for.</p>
                        <Link href="/">
                            <a className="whitespace-nowrap inline-flex rounded-md bg-white py-3 px-5 text-sm font-medium text-blue-900 hover:bg-white hover:shadow-lg hover:text-blue-900 mt-5">BACK TO HOME</a>
                        </Link>
                    </article>
                </div>
            </main>
        </>
    )
}