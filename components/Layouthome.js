import Footer from "./footerhome"
import Header from "./headerhome"

const Layout = ({ children }) => {
    return (
        <div className="overflow-x-hidden">
            <div className="max-w-1440 mx-auto">
                <Header />
                { children }
                <Footer />
            </div>
        </div>
    );
}

export default Layout;