//@ts-nocheck
import Image from 'next/image'
import Link from 'next/link'
import styles from '../../styles/Testimoni.module.scss'

function Testimoni() {
    return (
        <section className="section_advfm testimoni">
            <div className="w-full mb-5">
                <h2 className="text-white text-xl">Testimonial</h2>
                <h3 className="text-white text-xl">What our customers have to say</h3>
            </div>
        </section>
    )
}

export default Testimoni