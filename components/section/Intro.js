//@ts-nocheck
import Image from 'next/image'
import Link from 'next/link'
import styles from '../../styles/Home.module.css'
import elementWavesYellow from '../../public/elementWavesYellow.png'
import elementOrang from '../../public/elementOrang.png'
import elementWavesBlue from '../../public/elementWavesBlue.png'
import elementLineYellow from '../../public/elementLineYellow.png'

function Intro() {
    return (
        <section className="section_advfm intro">
            <div className="flex">
                <div className="flex-1 mx-auto relative">
                    <div className="flex flex-row relative justify-between items-center">
                        <div className="relative flex w-full">
                            <div className="relative flex w-full flex-wrap justify-between items-center">
                                <div className={styles.elementWavesYellow}>
                                    <div className={styles.btMediaImage}>
                                        {/* <img loading="lazy" width="1148" height="1424" src="https://demo.phlox.pro/agency-modern/wp-content/uploads/sites/175/2020/06/Path-50-min.png" className="aux-attachment aux-featured-image aux-attachment-id-1135" alt="Path 50-min" data-ratio="0.81" data-original-w="1148" /> */}
                                        <Image 
                                            src={elementWavesYellow} 
                                            alt="Link Net" 
                                            title="Link Net"
                                            width="1148" height="1424"
                                        />
                                    </div>
                                </div>
                                <div className={styles.elementOrang}>
                                    <div className={styles.btMediaImage}>
                                        {/* <img loading="lazy" width="724" height="1088" src="https://demo.phlox.pro/agency-modern/wp-content/uploads/sites/175/2020/06/smart-businessman-6NTHUVQ.png" className="aux-attachment aux-featured-image aux-attachment-id-92" alt="smart-businessman-6NTHUVQ" data-ratio="0.67" data-original-w="724" /> */}
                                        <Image 
                                            src={elementOrang} 
                                            alt="Link Net" 
                                            title="Link Net"
                                            width="724" height="1088"
                                        />
                                    </div>
                                </div>
                                <div className={styles.elementWavesBlue}>
                                    <div className={styles.btMediaImage}>
                                        {/* <img loading="lazy" width="1427" height="1637" src="https://demo.phlox.pro/agency-modern/wp-content/uploads/sites/175/2020/06/Union-1-min.png" className="aux-attachment aux-featured-image aux-attachment-id-1137" alt="Union 1-min" data-ratio="0.87" data-original-w="1427" /> */}
                                        <Image 
                                            src={elementWavesBlue} 
                                            alt="Link Net" 
                                            title="Link Net"
                                            width="1427" height="1637"
                                        />
                                    </div>
                                </div>
                                <div className={styles.elementLineYellow}>
                                    <div className={styles.btMediaImage}>
                                        {/* <img loading="lazy" width="1987" height="1308" src="https://demo.phlox.pro/agency-modern/wp-content/uploads/sites/175/2020/06/Group-11-min.png" className="aux-attachment aux-featured-image aux-attachment-id-1136" alt="Group 11-min" data-ratio="1.52" data-original-w="1987" /> */}
                                        <Image 
                                            src={elementLineYellow} 
                                            alt="Link Net" 
                                            title="Link Net"
                                            width="1987" height="1308"
                                        />
                                    </div>
                                </div>
                                <section className="w-full wordingIntro relative">
                                    <div className="relative flex mx-auto">
                                        <div className="w-auto min-w-100">
                                            <h2 className="bt-adv-heading-primary">Advertising First Media</h2>
                                            <h3 className="text-white text-xl bt-adv-heading-secondary">
                                                <span className="bt-head-highlight">We Make Digital Products and Services</span>
                                                <span className="dot-adv-orange">.</span>
                                            </h3>
                                            <div className="bt-adv-heading-intro-description">
                                                <p>But I must explain to you how all this mistaken idea of denouncing and praising pain was born and i will give you a complete account of the.</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <div className={styles.elementWavesTop}>
                                    <div className={styles.btMediaImage}>
                                        <div className="">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="464" height="464" viewBox="0 0 464 464">
                                                <circle id="Ellipse_18" data-name="Ellipse 18" cx="232" cy="232" r="232" fill="var(--color-advPrimaryBLueDark)"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.elementLineYellowTop}>
                                    <div className={styles.btMediaImage}>
                                        <div className="">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="347.168" height="306.282" viewBox="0 0 347.168 306.282">
                                                <path id="Path_46" data-name="Path 46" d="M42.556,352.577l-.012-2.012c112.691-.644,200.3-32.223,260.4-93.862C389.07,168.365,387.718,47.547,387.7,46.335l2.012-.04c.022,1.217,1.407,122.839-85.31,211.795C243.911,320.139,155.814,351.929,42.556,352.577Z" transform="translate(-42.544 -46.295)" fill="var(--color-advOrange)"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Intro