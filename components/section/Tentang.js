//@ts-nocheck
import Image from 'next/image'
import Link from 'next/link'
import styles from '../../styles/Tentang.module.scss'
import elementCewek from '../../public/elementCewek.png'

function Tentang() {
    return (
        <section className={styles["section_tentang"]}>
            <div className="">
                <div className="flex">
                    <div className="">
                        <div className="">
                            <Image 
                                src={elementCewek} 
                                alt="Planing & Research" 
                                title="Planing & Research"
                                width="750" height="619"
                            />
                        </div>
                    </div>
                    <div className="">
                        <div className="">
                            <div className="">
                                <div className="">
                                    <div className="w-full mb-5">
                                        <h2 className="text-white text-xl">What we do</h2>
                                        <h3 className="text-white text-xl">Stop interrupting. Start inspiring with Boosted Shots.</h3>
                                        <div className="">
                                            <p>Advertise your brand organically within First Media Ads inspiration feed. Boosted Shots act just like regular shots designers post on our website – just way more visible.</p>
                                            <p>Whether driving awareness, announcing a product launch, or showcasing a special offer, you have control over the design, copy, CTA, and destination.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="">
                                    <Link href="/signup">
                                        <a className="whitespace-nowrap inline-flex rounded-md bg-adv-orange py-3 px-5 text-sm font-medium text-white hover:shadow-lg">Start now - Get in touch</a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Tentang