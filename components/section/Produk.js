//@ts-nocheck
import Image from 'next/image'
import Link from 'next/link'
import styles from '../../styles/Produk.module.scss'
import IcnPlaning from '../../public/IcnPlaning.svg'
import IcnInvestmentPlanning from '../../public/IcnInvestmentPlanning.svg'
import IcnBusinessConsultancy from '../../public/IcnBusinessConsultancy.svg'
import IcnCustomerService from '../../public/IcnCustomerService.svg'

function Produk() {
    return (
        <section className={`${styles["section_produk"]} ${styles["section_produks"]}`}>
            <div className="">
                <div className="">
                    <div className="">
                        <div className="">
                            <div className="w-full mb-5">
                                <h2 className={styles["bt-adv-heading-primary"]}>Produk</h2>
                                <h3 className={styles["bt-adv-heading-secondary"]}>
                                    <span className={styles["bt-head-highlight"]}>We Grow Your Business</span>
                                </h3>
                            </div>
                        </div>
                        <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-x-2 gap-y-4 xl:gap-x-8 xl:gap-y-10 mt-16 relative z-10">
                            <div className="">
                                <section className="rounded-3xl bg-transparent hover:bg-blue-PrimaryDark pt-10 px-10">
                                    <div className="">
                                        <Image 
                                            src={IcnPlaning} 
                                            alt="Planing & Research" 
                                            title="Planing & Research"
                                            width="103" height="103"
                                        />
                                    </div>
                                    <div className="pb-10">
                                        <h4 className="">Planing & Research</h4>
                                        <div className="">
                                            <p className="">But I must explain to you how all this mistaken idea of denouncing and praising pain was born of the.</p>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div className="">
                                <section className="rounded-3xl bg-transparent hover:bg-blue-PrimaryDark pt-10 px-10">
                                    <div className="">
                                        <Image 
                                            src={IcnInvestmentPlanning} 
                                            alt="Deeper Insights & Data" 
                                            title="Deeper Insights & Data"
                                            width="103" height="103"
                                        />
                                    </div>
                                    <div className="pb-10">
                                        <h4 className="">Deeper Insights & Data</h4>
                                        <div className="">
                                            <p className="">But I must explain to you how all this mistaken idea of denouncing and praising pain was born of the.</p>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div className="">
                                <section className="rounded-3xl bg-transparent hover:bg-blue-PrimaryDark pt-10 px-10">
                                    <div className="">
                                        <Image 
                                            src={IcnBusinessConsultancy} 
                                            alt="Business Consultancy" 
                                            title="Business Consultancy"
                                            width="103" height="103"
                                        />
                                    </div>
                                    <div className="pb-10">
                                        <h4 className="">Business Consultancy</h4>
                                        <div className="">
                                            <p className="">But I must explain to you how all this mistaken idea of denouncing and praising pain was born of the.</p>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div className="">
                                <section className="rounded-3xl bg-transparent hover:bg-blue-PrimaryDark pt-10    px-10">
                                    <div className="">
                                        <Image 
                                            src={IcnCustomerService} 
                                            alt="Customer Service" 
                                            title="Customer Service"
                                            width="103" height="103"
                                        />
                                    </div>
                                    <div className="pb-10">
                                        <h4 className="">Customer Service</h4>
                                        <div className="">
                                            <p className="">But I must explain to you how all this mistaken idea of denouncing and praising pain was born of the.</p>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Produk