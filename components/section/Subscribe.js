//@ts-nocheck
import Image from 'next/image'
import Link from 'next/link'

function Subscribe() {
    return (
        <section className="section_advfm subscribe">
            <div className="w-full rounded-xl bg-adv-blue2 py-10 px-12">
                <div className="grid grid-cols-2 gap-4 justify-between items-center">
                    <div>
                        <h2 className="text-white text-xl font-medium">Dapatkan Informasi terbaru dari First Media Advertising</h2>
                    </div>
                    <div className="flex items-center py-2 px-2 rounded-xl bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0">
                        <input type="email" className="block w-full rounded-md bg-gray-100 border-transparent focus:border-0 focus:shadow-none focus:border-transparent focus:ring-0 focus:outline-none text-gray-500" placeholder="Masukan email Anda..." />
                        <button className="whitespace-nowrap inline-flex rounded-md bg-adv-orange py-3 px-5 text-sm font-medium text-white hover:shadow-lg" type="button">Subscribe</button>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Subscribe