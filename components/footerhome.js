//@ts-nocheck
import Image from 'next/image'
import Link from 'next/link'
import styles from '../styles/Footer.module.css'
import LogoLinknet from '../public/logo-linknet.png'
import AwardsLinknet from '../public/awards-linknet.png'

function Footer() {
    return (
        <footer className={styles.sectionFooterAdvFM}>
            <section className="px-6 md:px-10">
                <div className="flex flex-row justify-between items-center py-8">
                    <div className="flex flex-row items-center">
                        <Image 
                            src={LogoLinknet} 
                            alt="Link Net" 
                            title="Link Net"
                            width="100" height="24.61"
                        />
                    </div>
                    <div>
                        <p className="whitespace-nowrap inline-flex rounded-3xl bg-white py-2 px-5 text-sm font-medium text-blue-900 hover:bg-white hover:shadow-lg hover:text-blue-900">Back to top</p>
                    </div>
                </div>
                <div className="flex space-x-20 flex-row py-8">
                    <div>
                        <div>
                            <h2 className="text-white font-semibold mb-5">Produk</h2>
                        </div>
                        <div>
                            <nav>
                                <ul className="flex-row ">
                                    <li className="mb-2">
                                        <Link href="https://www.linknet.co.id/">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Tentang LinkNet</a>
                                        </Link>
                                    </li>
                                    <li className="mb-2">
                                        <Link href="/api-linknet">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">API LinkNet</a>
                                        </Link>
                                    </li>
                                    <li className="mb-2">
                                        <Link href="/kebijakan-privasi">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Kebijakan Privasi</a>
                                        </Link>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div>
                        <div>
                            <h2 className="text-white font-semibold mb-5">Perusahaan</h2>
                        </div>
                        <div>
                            <nav>
                                <ul className="flex-row ">
                                    <li className="mb-2">
                                        <Link href="https://www.linknet.co.id/">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Tentang LinkNet</a>
                                        </Link>
                                    </li>
                                    <li className="mb-2">
                                        <Link href="/kebijakan-privasi">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Kebijakan Privasi</a>
                                        </Link>
                                    </li>
                                    <li className="mb-2">
                                        <Link href="/karir">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">We Are Hiring!</a>
                                        </Link>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div>
                        <div>
                            <h2 className="text-white font-semibold mb-5">Dukungan</h2>
                        </div>
                        <div>
                            <nav>
                                <ul className="flex-row ">
                                    <li className="mb-2">
                                        <Link href="/bantuan">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Bantuan</a>
                                        </Link>
                                    </li>
                                    <li className="mb-2">
                                        <Link href="/dukungan-kontak">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Dukungan Kontak</a>
                                        </Link>
                                    </li>
                                    <li className="mb-2">
                                        <Link href="/faq">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">FAQ</a>
                                        </Link>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div>
                        <Image 
                            src={AwardsLinknet} 
                            alt="Awards Link Net" 
                            title="Awards Link Net"
                            width="507" height="181.66"
                        />
                    </div>
                </div>
                <div className="">
                    <div className="">
                        <p className="text-white text-sm py-3">© 2021 Advertising First Media • Didukung oleh PT Link Net Tbk.</p>
                    </div>
                </div>
            </section>
        </footer>
    )
}

export default Footer