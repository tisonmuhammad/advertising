//@ts-nocheck
import Image from 'next/image'
import LogoAdvFM from '../public/Logo-FM-ads.png'
import Link from 'next/link'

function Header() {
    return (
        <div className="bt-adv-overlay-header px-6 md:px-10 overflow-hidden">
            <div className="max-w-1440 mx-auto">
                <header className="max-w-1440 mx-auto py-5 mb-5 relative z-50">
                    <div className="flex flex-row justify-between items-center text-tblack">
                        <div className="flex flex-row items-center">
                            <a href="" className="mr-10 flex-none" title="homepage">
                                {/* <img src="/Logo-FM-ads.png" alt="Logo Advertising First Media" className="" width="80" height="48" title="Logo Advertising First Media" /> */}
                                <Image 
                                    src={LogoAdvFM} 
                                    alt="Advertising First Media" 
                                    title="Advertising First Media"
                                    width="80" height="48"
                                />
                            </a>
                            <nav>
                                <ul className="flex space-x-6 flex-row ">
                                    <li>
                                        <Link href="/produk">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Produk</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="/tentang-kami">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Tentang Kami</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="/klien">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Klien</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="/blog">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Blog</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href="/kontak">
                                            <a className="text-white text-opacity-50 hover:text-opacity-100">Kontak</a>
                                        </Link>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div>
                            <ul className="flex space-x-4 flex-row items-center">
                                <li>
                                    <Link href="/sign_in">
                                        <a className="whitespace-nowrap inline-flex rounded-md bg-transparent py-3 px-5 text-sm font-medium text-white hover:bg-white hover:shadow-lg hover:text-blue-900">Sign in</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/sign_up">
                                        <a className="whitespace-nowrap inline-flex rounded-md bg-adv-orange py-3 px-5 text-sm font-medium text-white hover:shadow-lg">Sign up</a>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </header>
            </div>
        </div>
    );
}

export default Header